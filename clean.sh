#!/usr/bin/env bash

rm -f *.db

find -type f -name "*.o" | xargs rm -f {}